package decorator;

public class Cotizante implements Usuario {

    String nombresApellidos = "TattoMolina";
    String documento = "1234";
    String genero = "Masculino";

    public String getNombresApellidos() {
        return nombresApellidos;
    }

    public void setNombresApellidos(String nombresApellidos) {
        this.nombresApellidos = nombresApellidos;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    

    @Override
    public String showUsuario() {
        return "NombreApellidos:TattoMolina, Documento: 1234, Género: Masculino, DireccionAgregada: ";
    }
    
}
