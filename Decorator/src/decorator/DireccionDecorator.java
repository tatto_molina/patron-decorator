package decorator;

public class DireccionDecorator extends UsuarioDecorator
{
    public DireccionDecorator  (Usuario specialUsuario)
    {
        super(specialUsuario);
    }

    public String showUsuario()
    {
        return specialUsuario.showUsuario() + agregarDireccion();
    }
    
    private String agregarDireccion()
    {
        return " + Decorator - Direccion";
    }
}
