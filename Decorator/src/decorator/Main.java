package decorator;

public class Main {
    
    public static void main(String[] args){
    Usuario usuario = new DireccionDecorator(new Cotizante());
    
    System.out.println(usuario.showUsuario());
}
}
