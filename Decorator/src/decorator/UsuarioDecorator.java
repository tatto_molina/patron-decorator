package decorator;

abstract class UsuarioDecorator implements Usuario{
    protected Usuario specialUsuario;
    
    public UsuarioDecorator (Usuario specialUsuario){
        this.specialUsuario= specialUsuario;
    }
    
    public String showUsuario(){
        return specialUsuario.showUsuario();
    }
}
